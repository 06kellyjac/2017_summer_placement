# 2017 - Summer Placement

[![pipeline status](https://gitlab.com/06kellyjac/2017_summer_placement/badges/master/pipeline.svg)](https://gitlab.com/06kellyjac/2017_summer_placement/commits/master)

This repository contains my presentation I ran at the National Software Academy about my Summer Placement @ Codeherent.

It's written in `LaTeX` with `beamer`; to view the presentation open the `.pdf` file in a PDF viewer.
