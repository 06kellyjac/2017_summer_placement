$xelatex = "xelatex %O %S";
$pdflatex = "xelatex %O %S";
$pdf_mode = "1";
$bibtex_use = 2;

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('glo-abr', 'gls-abr', 0, 'run_makeglossaries');

sub run_makeglossaries {
	if ( $silent ) {
		system "makeglossaries -q '$_[0]'";
	}
	else {
		system "makeglossaries '$_[0]'";
	};
}

push @generated_exts, 'nav', 'snm';
push @generated_exts, 'glg-abr', 'glo-abr', 'gls-abr';
push @generated_exts, 'glg', 'glo', 'gls';
push @generated_exts, 'acn', 'acr', 'alg';
$clean_ext .= ' %R.ist %R.xdy';
