#!/usr/bin/env sh
set -eux

URL="https://github.com/mozilla/Fira/archive/4.202.tar.gz"

apt-get update
apt-get install -y wget
wget "$URL" -P /tmp
tar -C /tmp -xf /tmp/4.202.tar.gz Fira-4.202/ttf
mv /tmp/Fira-4.202/ttf /usr/share/fonts/truetype/Fira
fc-cache -fv
